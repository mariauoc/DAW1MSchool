<!DOCTYPE html>
<!-- Primer ejemplo de acceso a MySQL desde PHP
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Primer ejemplo BBDD</title>
    </head>
    <body>
        <h1>Stucom, la aplicación para registrar alumnos</h1>
        <p></p>
        <p><a href="altaAlumno.php">Registrar un alumno</a></p>
        <p><a href="altaProyecto.php">Registrar un proyecto</a></p>
        <p><a href="modificarNota.php">Modificar nota de un proyecto</a></p>
        <p><a href="verAlumnos.php">Ver todos los datos de los alumnos</a></p>
    </body>
</html>
