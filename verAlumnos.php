<!DOCTYPE html>
<!--
Página que muestra todos los datos de los alumnos
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Alumnos</title>
    </head>
    <body>
        <?php
        // incluimos el fichero de la bbdd
        require_once 'bbdd.php';
        // Llamamos a la función y recogemos el resultado
        $alumnos = selectAllAlumnos();
        
        echo "<table>";
        echo "<tr>";
        echo "<th>Código</th><th>Nombre</th><th>Apellidos</th><th>Edad</th><th>Género</th>";
        echo "</tr>";
        // Recorremos el array - resultado (alumnos) hasta que no haya más filas
        while ($fila= mysqli_fetch_assoc($alumnos)) {
            echo "<tr>";
            // Otra manera de mostrar los datos de una fila
            foreach ($fila as $dato) {
                echo "<td>$dato</td>";
            }
            
            
//            // extraigo los datos del array $fila
//            extract($fila);
//            // Crea las variables con los nombres de los campos ($code, $name, etc..)
//            echo "<td>$code</td> <td>$name</td> <td>$surname</td> <td>$age</td> <td>$gender</td>";
            echo "</tr>";
        }
        
        echo "</table>";
        ?>
    </body>
</html>
