<!DOCTYPE html>
<!--
Modificar nota de un proyecto
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Modificar nota</title>
    </head>
    <body>
        <?php
        require_once 'bbdd.php';
        ?>
        <form method="POST">
            Escoge el proyecto: <select name="proyecto">
                <?php
                $proyectos = selectIdProjects();
                while ($fila = mysqli_fetch_assoc($proyectos)) {
                    echo "<option>";
                    echo $fila["idproject"];
                    echo "</option>";
                }
                ?>
            </select>
            <br>
            Nueva nota: <input type="number" name="nota" min="0" max="10">
            <br>
            <input type="submit" name="boton" value="Modificar">
        </form>
        <?php
        if (isset($_POST["boton"])) {
            $id = $_POST["proyecto"];
            $nota = $_POST["nota"];
            $resultado = modificarNotaProyecto($id, $nota);
            if ($resultado == "ok") {
                echo "Nota actualizada";
            } else {
                echo "ERROR: $reusltado";
            }
        }
        ?>
    </body>
</html>
