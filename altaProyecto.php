<!DOCTYPE html>
<!--
Página para dar de alta un proyecto
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Proyecto</title>
    </head>
    <body>
        <?php
        require_once 'bbdd.php';
        ?>
        <form method="POST">
            <p>Nombre: <input type="text" name="nombre"></p>
            <p>Fecha: <input type="date" name="fecha"></p>
            <p>Nota: <input type="number" name="nota" min="0" max="10"></p>
            <p>
                Alumno: <select name="alumno">
                    <?php
                    $codigos = selectCodeAlumnos();
                    while ($fila = mysqli_fetch_assoc($codigos)) {
                        echo "<option>";
                        echo $fila["code"];
                        echo "</option>";
                    }
                    ?>
                </select>
            </p>
            <input type="submit" value="Alta" name="boton">
        </form>
        <?php
        if (isset($_POST["boton"])) {
            $nombre = $_POST["nombre"];
            $fecha = $_POST["fecha"];
            $nota = $_POST["nota"];
            $alumno = $_POST["alumno"];
            $result = insertarProyecto($nombre, $fecha, $nota, $alumno);
            if ($result == "ok") {
                echo "Proyecto dado de alta";
            } else {
                echo "ERROR: $result";
            }
        }
        ?>
    </body>
</html>
