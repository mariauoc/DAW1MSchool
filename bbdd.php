<?php

/* 
 * Fichero que va a tener todas las funciones que tengan que ver
 * con la base de datos
 */

// Función que devueve todos los id de los proyectos
function selectIdProjects() {
    $c = conectar();
    $select = "select idproject from project order by idproject";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}

// Función que devuelve los códigos de los alumnos registrados
function selectCodeAlumnos() {
    $c = conectar();
    $select = "select code from student";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return $resultado;
}

// Función que devuelve todos los datos de todos los alumnos registrados
function selectAllAlumnos() {
    // conecta a la bbdd
    $c = conectar();
    // Preparar la consulta sql que necesitamos
    $select = "select * from student";
    // Ejecutamos la consulta y recogemos el resultado
    $resultado = mysqli_query($c, $select);
    // desconectamos
    desconectar($c);
    // devolvemos el resultado
    return $resultado;
}

// Función que modifica la nota de un proyecto
function modificarNotaProyecto($idproyecto, $nuevanota) {
    $c = conectar();
    $update = "update project set mark=$nuevanota where idproject = $idproyecto";
    if (mysqli_query($c, $update)) {
        $resultado = "ok";
    } else {
        $resultado = mysqli_error($c);
    }
    desconectar($c);
    return $resultado;
}

// Función que inserta un proyecto de un alumno en la bbdd
function insertarProyecto($nombre, $fecha, $nota, $alumno){
    $c = conectar();
    $insert = "insert into project values (null, '$nombre', '$fecha', $nota, $alumno)";
    if (mysqli_query($c, $insert)) {
        $resultado = "ok";
    } else {
        $resultado = mysqli_error($c);
    }
    desconectar($c);
    return $resultado;
}
 
// Función que inserta los datos de un alumno en la bbdd
function insertarAlumno($codigo, $nombre, $apellido, $edad, $genero) {
    // conecta a la bbdd
    $conexion = conectar();
    // Preparamos la sentencia sql que necesitamos
    $insert = "insert into student values ($codigo, '$nombre', '$apellido',
        $edad, '$genero')";
    // Ejecutamos la consulta
    if (mysqli_query($conexion, $insert)) {
        // Si ha ido todo bien devuelvo ok
        $resultado = "ok";
    } else {
        // Si ha ido mal, devolvemos el error de mysql
        $resultado = mysqli_error($conexion);
    }
    // Cerramos la conexión
    desconectar($conexion);
    return $resultado;
    
}

// Función que cierra la conexión con la base de datos
function desconectar($conexion) {
    mysqli_close($conexion);
}

// Función que conecta a la base de datos school
function conectar() {
    // Establecemos conexión con la bbdd
    $conexion = mysqli_connect("localhost", "root", "root", "school");
    // Si no hay conexión
    if (!$conexion) {
        // Interrumpimos aplicación con msg de error
        die("No se ha podido establecer la conexión con el servidor");
    }
    return $conexion;
}

